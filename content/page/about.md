---
title: About me
subtitle: Hugo Nijhuis-Mekkelholt
comments: false
---

Do you love trying new technology? Always looking for new ways to tackle challenges? That’s great because so do I!

I started this blog to share my experiences with whoever might be interested.

### How it all began..

Born in 87 it was my grandfather who put me infront of a computer before I reached the age of 1. 
On my 7th birthday I got my first computer and I started programming when I was 10 and it’s all history from there.

Want to reach out to me with any inquiries just use one of the options in the footer of this site!
