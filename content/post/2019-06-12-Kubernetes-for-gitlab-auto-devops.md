---
title: Setting up Kubernetes
subtitle: for my open source gitlab projects
date: 2019-06-12
tags: ["gitlab","kubernetes"]
draft: true
---

I tend to dable in various technological solutions to keep my hobby projects organized. From 
hosting my own git or GitLab CE server to Microsoft’s VSTS to GitHub or even the occasional
BitBucket repository. Recently I’ve landed on using GitLab mainly for my projects. I used to have
a DigitalOcean managed Kubernetes cluster to host my projects. However I decided to give the
built-in Google Kubernetes that GitLab has a go to finally set up a proper separate development
and production environment.

### goals
..small text

* set up a separate kubernetes cluster for development and production
* use google cloud
* reuse the same load balancer to save costs
* use Gitlab’s auto devops feature
* use the development environment for review apps
* use the production environment for production deployments
* reuse the clusters for multiple projects

### creating the development kubernetes cluster

Creating the development cluster is a simple next, next and next proces using GitLab. 
Simply navigate to one of your projects in GitLab, go to the *Operations* tab and under that navigate to
the *Kubernetes* menu option. In the middle you should see a green button *Add Kubernetes cluster*.

![Add Kubernetes cluster](/img/add-cluster.jpg)

After you click on *Add Kubernetes cluster* you will need to supply it with some information. You have 2 options, either 
create a new cluster on GKE or add an existing cluster. I’m gonna go with the create new cluster for this first one.
fill in the cluster name, I went with senzible-dev to denote this will be the dev environment.
Leave the environment on *\**, you could also go with *review/\** to prevent any other deployments from
running on this cluster. Then configure the desired region, node count and machine type.
Small tip: you can always edit the node pools later in Google Cloud Console. I personally changed it later to use
preempible instances to reduce the costs of this my non-critical hobby deployments. After you are happy with your
configuration click on *Create new cluster*. After a few minutes your cluster will be
up and running.

### setting up ingress and automatic ssl provisioning for your deployments
..todo

### creating the production cluster

Creating the production cluster is as simple as repeating the steps for the development cluster.
Just be sure to set the environment in this case to *production*. 
Another thing to be mindfull of is to log onto the same Google Billing account. This way when you add Helm and Ingress
to the production cluster it will add a Load balancer forwarding rule to the same project.
In Google cloud you pay a fixed price for the first 5 forwarding rules, if however you specify a different 
billing account / project. That will double your Load Balancing costs.

### reuse the clusters for another project

..todo